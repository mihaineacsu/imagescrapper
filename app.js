var cliArgs = require('command-line-args'),
	fs = require('fs'),
	argv = require('minimist')(process.argv.slice(2)),
    moment = require('moment'),
    mkdirp = require('mkdirp'),
    request = require('request');

var cli = cliArgs([
    { name: "help", type: Boolean, description: "Print usage instructions" },
    { name: "verbose", type: Boolean, description: "Write plenty output, verbose mode" },
    { name: "order", type: Boolean, description: "Stores images in a folder named after the date and time of when the script was ran" },
    { name: "start", type: Number, defaultOption: 0, description: "Lower limit, starting ID to request; if omitted it defaults to 0" },
    { name: "end", type: Number, defaultOption: 100000, description: "Upper limit at which requests end; if omitted it defaults to 100000" }
]);

var usage = cli.getUsage({
    header: "Image scrapper",
    footer: "For more information, contact me at mihai.mneacsu@gmail.com"
});

if (argv['help']){
	console.log(usage);
	process.exit(0);
}


var startID = argv['start'] || 0;
var endID = argv['end'] || 100000;
var orderByDate = argv['order'] || false;
var verbose = argv['verbose'] || false;

var folderName = './images/';
if (orderByDate)
	folderName += moment().format('MM_DD__HH_mm_ss') + '/';
else
	folderName += 'unordered' + '/';

mkdirp(folderName, function(err) {
	if (err){
		console.log('Error creating the folder');
		process.exit(1);
	}

	console.log('Storing images in ' + folderName);
});

var link = 'http://www.winebow.com/trade_detail.php?id=';
var errorMsgLength = 'Invalid Parameter'.length;
var toSearch = 'href="';

var step = 20;
var counter;

function download(i){
	counter = 0;

	if (verbose)
		console.log("Reached request no. " + i);

	var upperScope = endID;
	if ((i + step) < endID)
		upperScope = i + step;

	for (; i < upperScope; ++i){
		makeRequest(i, upperScope);
	}
}

function makeRequest(i, upperScope){
	var index = i;

	request(link + index, function (error, response, body) {
		if (!error &&
			response.statusCode == 200 &&
			body.length > errorMsgLength) {
				var imgIndex = body.indexOf('mg');
				if (imgIndex > 0){
					body = body.substring(0, imgIndex);
					body = body.substring(body.lastIndexOf(toSearch) + toSearch.length);
					var imageLink = body.substring(0, body.indexOf('"'));

					if (imageLink.length > 0){
						var brand = imageLink.substring(imageLink.indexOf('brand'));
						var filenameIndex = brand.indexOf('/');
						var filename = brand.substring(filenameIndex + 1);
						brand = folderName + brand.substring(0, filenameIndex);

						mkdirp(brand, function(err){
							if (err){
								console.log('Error creating folder');
								process.exit(1);
							}

							var req = request(imageLink);
							req.on('response', function(resp){
								if (resp.statusCode === 200)
									req.pipe(fs.createWriteStream(brand + '/' + filename, {flags: 'w'}));
								incCounter(upperScope);
							});
							req.on('error', function(err){
								console.log('Error event, restarting request for id ' + index);
								console.log(err.stack);
								download(index, upperScope);
							});

						});
					} else
						incCounter(upperScope);
				} else
					incCounter(upperScope);
		} else
			incCounter(upperScope);
	});
}

function incCounter(i){
	++counter;
	if (counter == step)
		download(i);
}

download(startID);
