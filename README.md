# Image Scarpper

### Installation
```npm install```

### Run
```node app.js```

 ### Usage
```
  --help            Print usage instructions
  --verbose         Write plenty output, verbose mode
  --order           Stores images in a folder named after the date and time of when the script was ran
  --start <number>  Lower limit, starting ID to request; if omitted it defaults to 0
  --end <number>    Upper limit at which requests end; if omitted it defaults to 100000
    For more information, contact me at mihai.mneacsu@gmail.com
```

### Eg.
```node app.js --start 10 --end 1000 --verbose --order``` - will run the script from id 10 up to id 999, in verbose mode, storing the images in a folder name with named after local date and time

```node app.js --end 1000 --verbose``` - will run the script from id 0 up to id 999, in verbose mode, storing the images in images/unordered

```node app.js``` - will run the script from id 0 up to id 100 000


**For more information, contact me at mihai.mneacsu@gmail.com**
